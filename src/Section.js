import React from 'react';
import './Section.css';


export function Section(props) {
    const {children}=props;
    return (
        <div className="Section">
            {children}
        </div>)
}

export function Row(props){
    const {label,children}= props;
    return(
        <div className="Row">
            <div className="labelContainer">
                <h1>{label} :</h1>
            </div>
            <div className="children">
                {children}
            </div>
        </div>
    )
}