import React,{useState, useEffect} from 'react';
import './Greeting.css'
import {Section,Row} from './Section';


export default function Greeting(props){
    const name=useFormInput('Jeremy');
    const surname= useFormInput('Dumont');
    const width=useWindowWidth();
    const height=useWindowHeight();
    const small=useMedia("(max-width: 500px)");
    const large=useMedia("(min-width: 1000px)");
    useDocumentTitle(name.value+" "+surname.value)
    return(
    <Section>
        <Row label="Name" >
            <input 
            className="Greeting"
            {...name}
            />
        </Row>
        <Row label="Surname" >
            <input 
            className="Greeting"
            {...surname}
            />
        </Row>
        <Row label="Size">
            <h1 className="sizeTitle">Change screen size!</h1>
            <p>{width}x{height}</p>
            <p>
                Small? {small?"yes":"no"}
            </p>
            <p>
                Large? {large?"yes":"no"}
            </p>
        </Row>
      </Section>
    )
}

function useMedia(query){
    const [matches,setMatches]=useState(
        window.matchMedia(query).matches
        );

    useEffect(
        ()=>{
            let media=window.matchMedia(query);
        if(media.matches !== matches){
            setMatches(matches);
        }
        let listener= () => setMatches(media.matches);
        media.addListener(listener);
        return () => media.removeListener(listener);
        },
        [matches,query]
    );
    return matches;


}


function useFormInput(inputValue){
    const [value,setValue]=useState(inputValue);
    function handleChange(e){
        setValue( e.target.value)
    };
    return {value,
            onChange:handleChange,
            };
}

function useDocumentTitle(title){
    useEffect(()=>{
        document.title=title;
    });
}

function useWindowWidth(){
    const [width,setWidth]= useState(window.innerWidth);
    useEffect(()=>{
        const handleResize = () => setWidth(window.innerWidth);
        window.addEventListener('resize',handleResize);
        return () => {
            window.removeEventListener('resize',handleResize)
        };
    })
    return width;
}

function useWindowHeight(){
    const [height,setHeight]= useState(window.innerHeight);
    useEffect(()=>{
        const handleResize = () => setHeight(window.innerHeight);
        window.addEventListener('resize',handleResize);
        return () => {
            window.removeEventListener('resize',handleResize)
        };
    })
    return height;
}